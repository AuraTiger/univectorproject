#include"Vector.h"

#include <iostream>

Vector::Vector() {
    vec = new Vec{0, 0, 0};
    start = Point();
    end = Point();
}

Vector::Vector(double x, double y, double z) {
    vec = new Vec{x, y, z};
    start = Point();
    end = Point(x, y, z);
}

Vector::Vector(const Point &p1,const Point &p2) {
    vec = new Vec{p2.getX() - p1.getX(),
                  p2.getY() - p1.getY(),
                  p2.getZ() - p1.getZ()};

    start = Point(p1.getX(), p1.getY(), p1.getZ());
    end = Point(p2.getX(), p2.getY(), p2.getZ());
}

//copy constructor
Vector::Vector(const Vector &var) {

    if(vec != nullptr){
        delete vec;
        vec = new Vec{var.getX(), var.getY(), var.getZ()};
    }

    if(direction != nullptr){
        delete direction;
        direction = new Vec{var.direction->x, var.direction->y, var.direction->z};
    }

    length = var.length;

    start = var.start;
    end = var.end;

}

Vector::~Vector() {
    if (direction != nullptr) {
        delete direction;
        direction = nullptr;
    }

    if (vec != nullptr) {
        delete vec;
        vec = nullptr;
    }
}

double Vector::getX(void) const {
    return vec->x;
}

double Vector::getY(void) const {
    return vec->y;
}

double Vector::getZ(void) const {
    return vec->z;
}

Point Vector::getStart(void) const {
    //Point *start = new Point(*start);  I removed this line bc it was breaking the calculateAngle function
    return start;
}

Point Vector::getEnd(void) const {
    //Point *end = new Point(*end); look line 70
    return end;
}

Point Vector::getMiddle(void) const {
    return Point((start.getX() + end.getX()) / 2,
                 (start.getY() + end.getY()) / 2, 
                 (start.getZ() + end.getZ()) / 2);
}

double Vector::calculateLength() const {
    if (length < 0) {
        return sqrt(pow(vec->x, 2) + pow(vec->y, 2) + pow(vec->z, 2));
    }
}

Vector *Vector::calculateDirection(void) {
    if (calculateLength() == 0) {
        throw VectorLengthException();
    }

    return   new Vector(vec->x / calculateLength(),
                                        vec->y / calculateLength(),
                                        vec->z / calculateLength());
}

bool Vector::isZeroVector(void) const {
    //if(vec->x == 0 && vec->y == 0 && vec->z == 0){
    //        return true;
    //    }
    //    return false;
    //I changed this. Hope you don't mind. - Dimi
    return vec->x == 0 && vec->y == 0 && vec->z == 0;
}

bool Vector::isParallelWith(const Vector &var) const {
    if (isZeroVector() || var.isZeroVector()) {
        throw VectorLengthException();
    }

    // the formula for finding if the vector is parallel is:
    // x/v1 == y/v2 == z/v3
    // However this is dangerous because we can have 0 division x/0!!
    // To fix the problem we need to change the formula to use multiplication

    return (vec->x * var.getY() == vec->y * var.getX()) &&
           (vec->y * var.getZ() == vec->z * var.getY()) &&
           (vec->x * var.getZ() == vec->z * var.getX());

    // using division
    // return (vec->x / var.getX()) == (vec->y / var.getY()) &&
    //     (vec->y / var.getY()) == (vec->z / var.getZ());
}

bool Vector::isPerpendicularWith(const Vector &var) const {

    if (isZeroVector() || var.isZeroVector()) {
        throw VectorLengthException();
    }

    return (*this * var) == 0;
}

Vector& Vector::operator=(const Vector& var){
    if(this != &var){
        length = var.length;
        start = var.start;
        end = var.end;

        if(vec != nullptr){
            delete vec;
            vec = new Vec{var.getX(), var.getY(), var.getZ()};
        }

        if(direction != nullptr){
            delete direction;
            direction = new Vec{var.direction->x, var.direction->y, var.direction->z};
        }
    }
    return *this;
}
bool Vector::isEqualWith(const Vector &var) const {
    if (this->getStart() == var.getStart() && this->getEnd() == var.getEnd())
        return true;

    return false;
}

Vector * Vector::operator+(const Vector & var) const{
    return new Vector(vec->x + var.getX(),
                      vec->y + var.getY(),
                      vec->z + var.getZ());
}

Vector *Vector::operator-(const Vector &var) const {
    return new Vector(vec->x - var.getX(),
                      vec->y - var.getY(),
                      vec->z - var.getZ());
}

Vector *Vector::operator*(double scalar) const {
    return new Vector(vec->x * scalar,
                      vec->y * scalar,
                      vec->z * scalar);
}

double Vector::operator*(const Vector &var) const {
    return (vec->x * var.getX() +
            vec->y * var.getY() +
            vec->z * var.getZ());
}

//cross product multiplication
Vector Vector::operator^(const Vector &var) const {
    return Vector((vec->y * var.getZ()) - (vec->z * var.getY()),
                      (-vec->x * var.getZ() + vec->z * var.getX()),
                      (vec->x * var.getY() - vec->y * var.getX()));
}


//mixed multiplication
double Vector::operator()(const Vector &var1, const Vector &var2) const {
    return (*this ^ var1) * var2;
}

std::ostream &operator<<(std::ostream &out, const Vector &var) {
    out << "Vector:{ x = " << var.getX() << ", y = " << var.getY() << ", z = " << var.getZ() << " }";
    return out;
}

std::ostream &operator<<(std::ostream &out, const Vector *var) {
    out << "Vector:{ x = " << var->getX() << ", y = " << var->getY() << ", z = " << var->getZ() << " }";
    return out;
}

