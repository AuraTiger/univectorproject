#ifndef VECTOR
#define VECTOR

#include"../point/Point.h"
#include<vector>
#include<exception>
#include<iostream>
#include<math.h>

class VectorLengthException : public std::runtime_error {

public:
    VectorLengthException(const char *message = "Vector of length 0 does not have a direction.")
            : std::runtime_error(message) {}
};

struct Vec {
    double x, y, z;
};

class Vector {
    friend std::ostream &operator<<(std::ostream &, const Vector &);

private:
    double length = -1;
    Point start;
    Point end; 
    Vec *direction = nullptr;
    Vec *vec = nullptr;

public:
    Vector();

    Vector(double, double, double);

    Vector(const Point &, const Point &);

    Vector(const Vector &);

    ~Vector();

    double getX(void) const;

    double getY(void) const;

    double getZ(void) const;

    Point getStart(void) const;

    Point getEnd(void) const;

    Point getMiddle(void) const;

    double calculateLength() const;

    Vector *calculateDirection(void);

    bool isZeroVector(void) const;

    bool isParallelWith(const Vector &) const;

    bool isPerpendicularWith(const Vector &) const;

    bool isEqualWith(const Vector &) const;

    Vector *operator+(const Vector &) const;

    Vector *operator-(const Vector &) const;

    Vector *operator*(double) const;

    double operator*(const Vector &) const;

    Vector operator^(const Vector &) const;

    double operator()(const Vector &, const Vector &) const;
    Vector& operator=(const Vector&);
};

#endif // VECTOR
