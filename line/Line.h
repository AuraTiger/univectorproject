#ifndef LINE
#define LINE

#include"../vector/Vector.h"
#include"../point/Point.h"

#include<cmath>
#define _USE_MATH_DEFINES

class Line : public Vector {
friend std::ostream &operator<<(std::ostream &, const Line &);

private:
    Point point;
    Vector vector;
public:
    Line();
    Line(Point &, Point &);
    Line(Point &, Vector &);
    virtual ~Line();

    Vector getVector() const;
    Point getPoint() const;

    Vector getDirection() const;
    Vector getNormal() const;
    double getAngle(const Line&) const;

    double calculatePointDistance(Point) const;

    bool operator+(const Point&) const;
    bool operator||(const Line&) const;
    bool operator==(const Line&) const;
    bool operator&&(const Line&) const;
    bool operator!=(const Line&) const;
    bool operator|(const Line&) const;

    Line& operator=(const Line&);
};

Line second_line(double, double, double, int);


#endif // LINE
