#include "Line.h"
#include"../vector/Vector.h"
#include "math.h"
#include"../point/Point.h"

#include "iostream"

using namespace std;

Line::Line(){
    point = Point();
    vector = Vector();
}

Line::Line(Point &x, Vector &a) {
    point = x;
    vector = a;
}

Line::Line(Point &x, Point &y) {
    point = x;
    vector = Vector(y.getX() - x.getX(), y.getY() - x.getY(), y.getZ() - x.getZ());
}

Line::~Line(){
}

Vector Line::getVector() const{
    Vector new_vector;
    double x = this->vector.getX();
    double y = this->vector.getY();
    double z = this->vector.getZ();

    new_vector = Vector(x, y, z);
    return new_vector;
}

Point Line::getPoint() const{
    Point a;

    double x = this->point.getX();
    double y = this->point.getY();
    double z = this->point.getZ();

    a = Point(x, y, z);
    return a;
}

double Line::calculatePointDistance(Point x) const
{
    Vector new_vector;

    Point current_point = this->getPoint();
    new_vector = Vector(current_point,  x);

    double distance = ((this->getVector() ^ new_vector).calculateLength() / this->getVector().calculateLength());
    return abs(distance);
}

Vector Line::getDirection() const {
    return this->getVector();
}

Vector Line::getNormal() const {
    Vector proj = Vector(this->getVector().getX() + 1, this->getVector().getY(), this->getVector().getZ());
     Vector cross_product = this->getVector() ^ proj;

    if (cross_product.calculateLength() != 0){
        return  this->getVector() ^ proj;
    }
    proj = Vector(this->getVector().getX() - 1, this->getVector().getY(), this->getVector().getZ());
    return this->getVector() ^ proj;
}

double Line::getAngle(const Line& x) const{
    return acos(this->vector * x.getVector() / (this->vector.calculateLength() * x.getVector().calculateLength())); // pi/2
}

Line& Line::operator=(const Line& x) {
    if (this != &x) {
        this->point = x.point;
        this->vector = x.vector;
    }
    return *this;
}

bool Line::operator+(const Point& x) const {
    // проверка дали дадена точка лежи на дадената права
    return this->calculatePointDistance(x) <= 0.1;
}

bool Line::operator||(const Line& x) const {
    // проверка дали дадена права е успоредна на друга права
    Vector proj = Vector(this->getPoint(), x.getPoint());
    return this->getVector().isParallelWith(x.getVector()) && ! proj.isZeroVector();
}

bool Line::operator==(const Line& x) const {
         // проверка дали права съвпада с друга права
        Vector proj = Vector(this->getPoint(), x.getPoint());
        return  this->getVector().isParallelWith(x.getVector()) && proj.isZeroVector();
}

bool Line::operator&&(const Line& x) const {
    // https://math.stackexchange.com/questions/697124/how-to-determine-if-two-lines-in-3d-intersect
    Vector n = (this->getVector() ^ x.getVector());
    Vector b = Vector(this->getPoint(), x.getPoint());

    bool result = n*b == 0;
    return result;
}

bool Line::operator!=(const Line& x) const {
    // проверка дали права е кръстосана с друга права
    return !(*this || x) && !(*this == x);
}

bool Line::operator|(const Line& x) const {
    // проверка дали права е перпендикулярна на друга права
    return this->getAngle(x) == M_PI / 2;
 }

Line second_line(double x, double y, double z, int n2){
    cout << "Please choose how to identify a line?\n1 - Via vector and point\n2 - Via two points" << endl;
            cin >> n2;
            Line line2; 
            while (n2 < 1 || n2 > 2)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }
            if (n2 == 1)
            {
                cout << "Please enter coordinates for point and vector!\nPoint:\n";
                cin >> x >> y >> z;
                Point point(x, y, z);
                cout << "Vector:\n";
                cin >> x >> y >> z;
                Vector vec_1(x, y, z);
                Line line_temp(point, vec_1);
                line2 = line_temp;
            }
            else
            {
                cout << "Please enter coordinates for two points!\nPoint 1:\n";
                cin >> x >> y >> z;
                Point point_start(x, y, z);
                cout << "Point 2:\n";
                cin >> x >> y >> z;
                Point point_end(x, y, z);
                Line line_temp(point_start, point_end);
                line2 = line_temp;
            }
            return line2;
}

std::ostream &operator<<(std::ostream &out, const Line &var) {
    out << "Line:{ Point = " << var.getPoint() << ", Vector = " << var.getVector() << " }";
    return out;
}

std::ostream &operator<<(std::ostream &out, const Line *var) {
    out << "Line:{ Point = " << var->getPoint() << ", Vector = " << var->getVector() << " }";
    return out;
}
