#ifndef TETRAHEDRON
#define TETRAHEDRON

#include"../vector/Vector.h"
#include"../shape/Shape.h"
#include<iostream>
#include<cmath>

class Tetrahedron : public Shape {

    friend std::ostream &operator<<(std::ostream &, const Tetrahedron &);

private:
    Point A;
    Point B;
    Point C;
    Point D;

public:
    Tetrahedron(const Point &, const Point &, const Point &, const Point &);
    Tetrahedron(const Tetrahedron &);
    ~Tetrahedron();

    Point getA() const;

    Point getB() const;

    Point getC() const;

    Point getD() const;

    bool isRegular() const;

    bool isOrthogonal() const;

    double calculateSurroundingArea();

    double calculateVolume() const;

    bool operator<(const Point &) const;

    bool operator>(const Point &) const;

    bool operator==(const Point &) const;
};

#endif