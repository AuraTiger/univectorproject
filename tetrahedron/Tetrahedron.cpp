#include"Tetrahedron.h"
#include"../triangle/Triangle.h"

Tetrahedron::Tetrahedron(const Point & A, const Point & B, const Point & C, const Point & D){
   try{
       if(A == B || A == C || A == D || B == C || B == D || C == D){
           throw (EqualPointException("Error while creating object Tetrahedron: Cannot have points of same value.\n"));
       }
   }catch(EqualPointException e){
       std::cout << e.getMessage() << std::endl;
   }

   this->A = A;
   this->B = B;
   this->C = C;
   this->D = D;
}

Tetrahedron::Tetrahedron(const Tetrahedron & t){
   A = t.A;
   B = t.B;
   C = t.C;
   D = t.D;
}

Tetrahedron::~Tetrahedron(){

}

Point Tetrahedron::getA() const {
   return A;
}

Point Tetrahedron::getB() const {
   return B;
}

Point Tetrahedron::getC() const {
   return C;
}

Point Tetrahedron::getD() const {
   return D;
}


bool Tetrahedron::isRegular() const {
   // Triangle t1(A, B, C);
   // Triangle t2(A, B, D);
   // Triangle t3(A, C, D);
   // Triangle t4(B, C, D);

   double t1 = (new Triangle(A, B, C))->calculateArea();
   double t2 = (new Triangle(A, B, D))->calculateArea();
   double t3 = (new Triangle(A, C, D))->calculateArea();
   double t4 = (new Triangle(B, C, D))->calculateArea();

   if(t1 == t2 && t2 == t3 && t3 == t4 && t4 == t1){
       return true;
   }

   return false;
}

bool Tetrahedron::isOrthogonal() const {
   //https://en.wikipedia.org/wiki/Orthocentric_tetrahedron

   // AB*AB + CD*CD = AC*AC + BD*BD = AD*AD + BC*BC

   double pair1 =
       (pow((B.getX() - A.getX()), 2) + pow((B.getY() - A.getY()), 2) + pow((B.getZ() - A.getZ()), 2)) +
       (pow((D.getX() - C.getX()), 2) + pow((D.getY() - C.getY()), 2) + pow((D.getZ() - C.getZ()), 2));

   double pair2 =
       (pow((C.getX() - A.getX()), 2) + pow((C.getY() - A.getY()), 2) + pow((C.getZ() - A.getZ()), 2)) +
       (pow((D.getX() - B.getX()), 2) + pow((D.getY() - B.getY()), 2) + pow((D.getZ() - B.getZ()), 2));

   double pair3 =
       (pow((D.getX() - A.getX()), 2) + pow((D.getY() - A.getY()), 2) + pow((D.getZ() - A.getZ()), 2)) +
       (pow((C.getX() - B.getX()), 2) + pow((C.getY() - B.getY()), 2) + pow((C.getZ() - B.getZ()), 2));

   if(pair1 == pair2 && pair2 == pair3){
       return true;
   }

   return false;
}

double Tetrahedron::calculateSurroundingArea() {
   double baseArea = (new Triangle(A, B, C))->calculateArea();
   Point middlePoint = (new Vector(A, B))->getMiddle();
   double slantHeight = (new Vector(middlePoint, D))->calculateLength();

   return (baseArea * slantHeight) / 2;
}

double Tetrahedron::calculateVolume() const {
   Vector DA(D, A);
   Vector DB(D, B);
   Vector DC(D, C);

   return (abs(DC(DB, DA)) / 6);
}

bool Tetrahedron::operator<(const Point & p) const {
    Tetrahedron ABCD(A, B, C, D);
    Tetrahedron ABDP(A, B, D, p);
    Tetrahedron ABCP(A, B, C, p);
    Tetrahedron ACDP(A, C, D, p);
    Tetrahedron BCDP(B, C, D, p);

    double sumOfAreas = (ABDP.calculateVolume() + ABCP.calculateVolume() + 
                         ACDP.calculateVolume() + BCDP.calculateVolume());

    if ((sumOfAreas - ABCD.calculateVolume()) < 0.001)
        return true;

    return false;
}

bool Tetrahedron::operator>(const Point & p) const {
    Tetrahedron ABCD(A, B, C, D);
    Tetrahedron ABDP(A, B, D, p);
    Tetrahedron ABCP(A, B, C, p);
    Tetrahedron ACDP(A, C, D, p);
    Tetrahedron BCDP(B, C, D, p);

    double sumOfAreas = (ABDP.calculateVolume() + ABCP.calculateVolume() + 
                         ACDP.calculateVolume() + BCDP.calculateVolume());

    if ((sumOfAreas - ABCD.calculateVolume()) < 0.001)
        return false;

    return true;
}

bool Tetrahedron::operator==(const Point & p) const {
    Tetrahedron ABDP(A, B, D, p);
    Tetrahedron ABCP(A, B, C, p);
    Tetrahedron ACDP(A, C, D, p);
    Tetrahedron BCDP(B, C, D, p);

    if (ABDP.calculateVolume() < 0.1 || ABCP.calculateVolume() < 0.1 || 
        ACDP.calculateVolume() < 0.1 || BCDP.calculateVolume())
        
        return true;

    return false;
}


std::ostream &operator<<(std::ostream &out, const Tetrahedron &var) {
    out << "Tetrahedron:{ A = " << var.getA() << ", B = " << var.getB() << ", C = " << var.getC() << ", D = " << var.getD() << " }";
    return out;
}

std::ostream &operator<<(std::ostream &out, const Tetrahedron *var) {
    out << "Tetrahedron:{ A = " << var->getA() << ", B = " << var->getB() << ", C = " << var->getC() << ", D = " << var->getD() << " }";
    return out;
}







