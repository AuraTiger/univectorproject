#include"Triangle.h"

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) {

    A = Point(p1);
    B = Point(p2);
    C = Point(p3);

    try {
        if (A == B)
            throw (EqualPointException("The triangle can't be created because A=B\n"));
        else if (B == C) {
            throw (EqualPointException("The triangle can't be created because B=C\n"));
        } else if (A == C) {
            throw (EqualPointException("The triangle can't be created because A=C\n"));
        }
    }
    catch (EqualPointException e) {
        std::cout << e.getMessage();
    }
}

Triangle::Triangle(const Vector *vec1, const Vector *vec2) {
    A = vec1->getStart();
    B = vec1->getEnd();
    C = vec2->getEnd();

    try {
        if (A == B)
            throw (EqualPointException("The triangle can't be created because A=B\n"));
        else if (B == C) {
            throw (EqualPointException("The triangle can't be created because B=C\n"));
        } else if (A == C) {
            throw (EqualPointException("The triangle can't be created because A=C\n"));
        }
    }
    catch (EqualPointException e) {
        std::cout << e.getMessage();
    }
}

Triangle::~Triangle() {
    //std::cout << "Deleting vertices\n";
}

Point Triangle::getA() const {
    return A;
}

Point Triangle::getB() const {
    return B;
}

Point Triangle::getC() const {
    return C;
}

double Triangle::calculateDistance(const Point &p1, const Point &p2) const {
    return std::sqrt(std::pow((p2.getX() - p1.getX()), 2) +
                     std::pow((p2.getY() - p1.getY()), 2) +
                     std::pow((p2.getZ() - p1.getZ()), 2));
}
double Triangle::calculateAngle(Vector &AB, Vector &BC, Vector &AC, std::string angle) const {
    double a = BC.calculateLength();
    double b = AC.calculateLength();
    double c = AB.calculateLength();

    if (angle == "alpha") {
        double angleAlpha = acos((b * b + c * c - a * a) / (2 * b * c));
        return angleAlpha;
    }
    if (angle == "beta") {
        double angleBeta = acos((a * a + c * c - b * b) / (2 * a * c));
        return angleBeta;
    }
    if (angle == "gama") {
        double angleGama = acos((c * c + a * a - b * b) / (2 * c * a));
        return angleGama;
    }

    return -1;
}


double Triangle::calculateArea() const {

    if (area >= 0) {    //if the area is already defined
        return area;
    }

    double tempA = calculateDistance(A, B);

    double tempB = calculateDistance(A, C);

    double tempC = calculateDistance(C, B);

    double semiP = calculatePerimeter() / 2;
    double result = std::sqrt(semiP * (semiP - tempA) * (semiP - tempB) * (semiP - tempC));

    return result;
}

double Triangle::calculatePerimeter() const {

    if (perimeter >= 0) {
        return perimeter;
    }

    double tempA = calculateDistance(A, B);

    double tempB = calculateDistance(A, C);

    double tempC = calculateDistance(C, B);

    return tempA + tempB + tempC;
}


std::string Triangle::getType() const {


    Vector AB(A, B);
    Vector BC(B, C);
    Vector AC(A, C);

    double alpha = calculateAngle(AB, BC, AC, "alpha");
    double beta = calculateAngle(AB, BC, AC, "beta");
    double gamma = calculateAngle(AB, BC, AC, "gamma");

    std::string answer = "";

    double rightAngle = 1.5708;

    if (alpha < rightAngle && beta < rightAngle && gamma < rightAngle)
        answer += "acute ";
    else if (alpha > rightAngle || beta > rightAngle || gamma > rightAngle)
        answer += "obtuse ";
    else
        answer += "right ";


    double lenAB = calculateDistance(A, B);
    double lenBC = calculateDistance(B, C);
    double lenCA = calculateDistance(C, A);

    if (lenAB == lenBC && lenBC == lenCA)
        answer += "equilateral";
    else if (lenAB == lenBC || lenBC == lenCA || lenAB == lenCA)
        answer += "isosceles";
    else
        answer += "scalene";


    return answer;
}

Point Triangle::getCentroid() const {

    double X = (A.getX() + B.getX() + C.getX()) / 3;
    double Y = (A.getY() + B.getY() + C.getY()) / 3;
    double Z = (A.getZ() + B.getZ() + C.getZ()) / 3;
    return Point(X, Y, Z);
}

bool Triangle::operator<(const Point &P) const {

    Triangle ABC(A, B, C);
    Triangle ABP(A, B, P);
    Triangle BCP(B, C, P);
    Triangle ACP(A, C, P);

    double sumOfAreas = (ABP.calculateArea() + BCP.calculateArea() + ACP.calculateArea());

    if ((sumOfAreas - ABC.calculateArea()) < 0.001)
        return true;

    return false;
}


bool Triangle::operator>(const Point &P) const {

    Triangle ABC(A, B, C);
    Triangle ABP(A, B, P);
    Triangle BCP(B, C, P);
    Triangle ACP(A, C, P);

    double sumOfAreas = (ABP.calculateArea() + BCP.calculateArea() + ACP.calculateArea());

    if ((sumOfAreas - ABC.calculateArea()) < 0.001)
        return false;

    return true;
}

bool Triangle::operator==(const Point &P) const {

    Triangle ABP(A, B, P);
    Triangle BCP(B, C, P);
    Triangle ACP(A, C, P);

    if (ABP.calculateArea() < 0.1 || BCP.calculateArea() < 0.1 || ACP.calculateArea() < 0.1)
        return true;

    return false;
}

std::ostream &operator<<(std::ostream &out, const Triangle &var) {
    out << "Triangle:{ A = " << var.getA() << ", B = " << var.getB() << ", C = " << var.getC() << " }";
    return out;
}

std::ostream &operator<<(std::ostream &out, const Triangle *var) {
    out << "Triangle:{ A = " << var->getA() << ", B = " << var->getB() << ", C = " << var->getC() << " }";
    return out;
}



