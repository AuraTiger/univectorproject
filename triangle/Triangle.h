#ifndef TRIANGLE
#define TRIANGLE

#include"../vector/Vector.h"
#include"../shape/Shape.h"
#include<iostream>
#include<cmath>

//    ISOSCELES, // равнобедрен 0
//    EQUILATERAL,    // равностранен 1
//    SCALENE,     //разностранен  2
//    ACUTE,  // остър 3
//    OBTUSE, // тъп 4
//    RIGHT,  // прав 5

// enum ANGLES {
//     ACUTE,
//     OBTUSE,
//     RIGHT,
//     ISOSCELES,
//     EQUILATERAL,
//     SCALENE
// };

class Triangle : public Shape {
    friend std::ostream &operator<<(std::ostream &, const Triangle &);

private:
    double area = -1;
    double perimeter = -1;
    Point A;
    Point B;
    Point C;

    double calculateDistance(const Point &p1, const Point &p2) const;

public:

    Triangle(const Point &, const Point &, const Point &);

    Triangle(const Vector *vec1, const Vector *vec2);

    ~Triangle();

    Point getA() const;

    Point getB() const;

    Point getC() const;

    double calculateAngle(Vector &, Vector &, Vector &, std::string) const;

    std::string getType() const;

    double calculateArea() const;

    double calculatePerimeter() const;

    Point getCentroid() const;

    bool operator<(const Point &) const;

    bool operator>(const Point &) const;

    bool operator==(const Point &) const;

};

#endif // TRIANGLE
