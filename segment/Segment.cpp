#include "Segment.h"
#include <iostream>

using namespace std;

Segment::Segment(Point &p1, const Vector &vec1) {
    start = p1;  //t=0
    Point tmpPoint(vec1.getX(), vec1.getY(), vec1.getZ());

    end = tmpPoint;
}

Segment::~Segment() {
   cout << "destructing" << endl;
}

double Segment::getLen() const {
    double x = end.getX() - start.getX();
    double y = end.getY() - start.getY();
    double z = end.getZ() - start.getZ();

    double distance = sqrt(x * x) + (y * y) + (z * z);
    return distance;
}

Point Segment::getMiddle() {
    double x = end.getX() - start.getX();
    double y = end.getY() - start.getY();
    double z = end.getZ() - start.getZ();

    return Point(x / 2, y / 2, z / 2);
}

bool Segment::operator==(Point &C) const {
    Point A = start;
    Point B = end;

    Vector AC(A, C);
    Vector CB(C, B);
    Vector AB(A, B);

    if ((AC.calculateLength() + CB.calculateLength()) == AB.calculateLength())
        return true;
    return false;
}

std::ostream &operator<<(std::ostream &out, const Segment &var) {
    out << "Segment:{ Point = " << var.getPoint() << ", Vector = " << var.getVector() << " }";
    return out;
}

std::ostream &operator<<(std::ostream &out, const Segment *var) {
    out << "Segment:{ Point = " << var->getPoint() << ", Vector = " << var->getVector() << " }";
    return out;
}