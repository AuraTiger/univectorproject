#ifndef SEGMENT
#define SEGMENT

#include"../line/Line.h"
#include"../point/Point.h"
#include"../vector/Vector.h"

class Segment : public Line {

    friend std::ostream &operator<<(std::ostream &, const Segment &);

public:
    Segment(Point &, const Vector &);

    virtual ~Segment();

    double getLen() const;

    Point getMiddle();

    bool operator==(Point &) const;

private:
    Point start;
    Point end;
};

#endif // SEGMENT
