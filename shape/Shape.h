#ifndef SHAPE
#define SHAPE

#include <string>
#include <exception>

class EqualPointException : public std::runtime_error {

public:
    EqualPointException(const std::string &message) : std::runtime_error(message) {
        this->message = message;
    }

    std::string getMessage() const {
        return message;
    }

private:
    std::string message;
};

class Shape {

};

#endif