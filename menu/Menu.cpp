#include "../point/Point.h"
#include "../vector/Vector.h"
#include "../line/Line.h"
#include "../triangle/Triangle.h"
#include "../tetrahedron/Tetrahedron.h"
#include "../segment/Segment.h"
#include <iostream>
#include <fstream>

using namespace std;

int printArray(int* arr, unsigned sz);
int main()
{

    int n;
    int n2;
    int x, y, z;

    do
    {
        cout << "Choose input method!\nEnter:\n1 - Console input\n2 - File input\n";
        cin >> n;
    } while (n != 1 && n != 2);
    //freopen("output.txt", "w", stdout);
    if (n == 1)
    {
        do
        {
            cout << "Select geometric figure:\n1 - Point\n2 - Vector\n3 - Line\n4 - Segment\n5 - Triangle\n6 - Tetrahedron\n";
            cin >> n;
        } while (n < 1 || n > 6);
        switch (n)
        {
        case 1:
        {

            cout << "Please enter x, y, z:" << endl;
            cin >> x >> y >> z;
            Point point1(x, y, z);
            Point point_def();

            cout << "Do you want to enter another point?(y/n)" << endl;
            char ans;
            cin >> ans;
            while (ans != 'y' && ans != 'n')
            {
                cout << "Please enter y for yes or n for no!" << endl;
                cin >> ans;
            }
            if (ans == 'y')
            {
                cout << "Please enter coordinates for second point!" << endl;
                cin >> x >> y >> z;
            }
            Point point2(x, y, z);
            cout << point1 << endl;
            cout << point2 << endl;
            break;
        }
        case 2:
        {
            Vector vector1;
            cout << "Please choose how to identify a vector?\n1 - Via vector coordinates\n2 - Via two points" << endl;
            cin >> n2;
            while (n2 < 1 || n2 > 2)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }
            if (n2 == 1)
            {
                cout << "Please enter vector coordinates!\n";
                cin >> x >> y >> z;
                Vector tmp(x, y, z);
                vector1 = tmp;
            }
            else
            {
                cout << "Please enter coordinates for two points!\nPoint1:\n";
                cin >> x >> y >> z;
                Point point_start(x, y, z);
                cout << "Point2:\n";
                cin >> x >> y >> z;
                Point point_end(x, y, z);
                Vector vec_temp(point_start, point_end);
                vector1 = vec_temp;
            }

            cout << "Select an action:\n1 - Vector length calculation\n2 - Vector direction calculation\n3 - Zero vector check\n4 - Checking for parallelism of two vectors\n5 - Perpendicularity check of two vectors\n6 - Collecting two vectors\n"
                << "7 - Subtraction of two vectors\n8 - Multiplication of a vector by a real number\n9 - Scalar product of two vectors\n10 - Vector product of two vectors\n11 - Mixed product of three vectors\n";
            cin >> n2;
            while (n2 < 1 || n2 > 11)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }

            switch (n2)
            {
            case 1:
                cout << "The length of: " << vector1 << ", is: " << vector1.calculateLength() << endl;
                break;
            case 2:
                try
                {
                    cout << vector1.calculateDirection() << endl;
                }
                catch (VectorLengthException& vec_ex)
                {
                    cerr << "Vector length = 0 \n";
                }
                break;
            case 3:
                cout << boolalpha << vector1.isZeroVector() << endl;
                break;
            case 4:
            {
                cout << "Please, enter coordinates for a vector: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                try
                {
                    cout << boolalpha << vector1.isParallelWith(vector2) << endl;
                }
                catch (VectorLengthException& vec_ex)
                {
                    cerr << "Vector length = 0 \n";
                }
                break;
            }
            case 5:
            {
                cout << "Please, enter coordinates for a vector: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                try
                {
                    cout << boolalpha << vector1.isPerpendicularWith(vector2) << endl;
                }
                catch (VectorLengthException& vec_ex)
                {
                    cerr << "Zero vector\n";
                }
                break;
            }

            case 6:
            {
                cout << "Please enter vector coordinates: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << *(vector1 + vector2) << endl;
                break;
            }
            case 7:
            {
                cout << "Please enter vector coordinates: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << *(vector1 - vector2) << endl;
                break;
            }

            case 8:
            {
                cout << "Please enter a number: " << endl;
                cin >> x;
                cout << *(vector1 * x) << endl;
                break;
            }
            case 9:
            {
                cout << "Please enter vector coordinates: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << vector1 * vector2 << endl;
                break;
            }
            case 10:
            {
                cout << "Please enter vector coordinates: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << (vector1 ^ vector2) << endl;
                break;
            }
            case 11:
            {
                cout << "Please enter coordinates for two vectors: \nVector 1: " << endl;
                cin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << "Vector 2 :" << endl;
                cin >> x >> y >> z;
                Vector vector3(x, y, z);
                cout << vector1(vector2, vector3) << endl;
                break;
            }
            }
            break;
        }
        case 3:
        {
            cout << "Please choose how to identify a line?\n1 - Via vector and point\n2 - Via two points" << endl;
            cin >> n2;
            Line line_1;
            while (n2 < 1 || n2 > 2)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }
            if (n2 == 1)
            {
                cout << "Please enter coordinates for point and vector!\nPoint:\n";
                cin >> x >> y >> z;
                Point point(x, y, z);
                cout << "Vector:\n";
                cin >> x >> y >> z;
                Vector vec_1(x, y, z);
                Line line_temp(point, vec_1);
                line_1 = line_temp;
            }
            else
            {
                cout << "Please enter coordinates for two points!\nPoint 1:\n";
                cin >> x >> y >> z;
                Point point_start(x, y, z);
                cout << "Point 2:\n";
                cin >> x >> y >> z;
                Point point_end(x, y, z);
                Line line_temp(point_start, point_end);
                line_1 = line_temp;
            }
            cout << "Select an action:\n1 - Lines direction\n2 - A normal vector\n3 - An angle between two lines\n4 - A point lies on another line\n5 - Parallel lines\n6 - Line matching\n7 - Intersects lines\n8 - Crossed lines\n9 - Perpendicular lines\n";
            cin >> n2;
            while (n2 < 1 || n2 > 9)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }
            switch (n2)
            {
            case 1:
            {
                //Vector vec_tmp = line1.getDirection();
                cout << "The direction of the line "
                    << " is " << line_1.getDirection() << endl;
            }
            break;
            case 2:
            {
                cout << "The normal vector of the given line is: " << line_1.getNormal() << endl;
            }
            break;
            case 3:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The angle btween the two lines is: " << line_1.getAngle(line2) << " rad.\n";
            }
            break;
            case 4:
            {
                cout << "Eneter a point:\n";
                cin >> x >> y >> z;
                Point point_lies(x, y, z);
                cout << "The point lies on the line: " << boolalpha << line_1 + point_lies << endl;
            }
            break;
            case 5:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The lines are parallel: " << boolalpha << (line_1 || line2) << endl;
            }
            break;
            case 6:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The l;ines overlap: " << boolalpha << (line_1 == line2);
            }
            break;
            case 7:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The lines are intersecting: " << boolalpha << (line_1 && line2) << endl;
            }
            break;
            case 8:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The lines are crossing: " << boolalpha << (line_1 != line2);
            }
            break;
            case 9:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "Line 1 is perpendicular to line 2: " << boolalpha << (line_1 | line2) << endl;
            }
            break;
            }
            break;
        }
        case 4:
        {
            cout << "Please enter point and vector coordinates!\nPoint:\n";
            cin >> x >> y >> z;
            Point point(x, y, z);
            cout << "Vector:\n";
            cin >> x >> y >> z;
            Vector vec_1(x, y, z);
            //Line line_temp(point, vec_1);
            cout << "Select an action:\n1 - Segment length\n2 - Segment middle point\n3 - Check if a point is a part of the given segment\n";
            cin >> n2;
            while (n2 < 1 || n2 > 3)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }
            Segment seg1(point, vec_1);
            switch (n2)
            {
            case 1:
            {
                cout << "The length of the segment is: " << seg1.getLen() << endl;
            }
            break;
            case 2:
            {
                //Point mid = seg1.getMiddle();
                cout << "The middle point of the segment is: " << seg1.getMiddle();
            }
            break;
            case 3:
            {
                //Point partPoint;
                cout << "Enter point coordinates!\n";
                cin >> x >> y >> z;
                Point partPoint(x, y, z);
                cout << "The point " << partPoint << " is part of the segment " << seg1 << ": " << boolalpha << (seg1 == partPoint) << endl;
            }

            break;
            }

            break;
        }
        case 5:
        {
            cout << "Please eneter coordinates of 3 points to create a triangle!\n";
            cout << "Point 1:\n";
            cin >> x >> y >> z;
            Point A(x, y, z);
            cout << "Point 2:\n";
            cin >> x >> y >> z;
            Point B(x, y, z);
            cout << "Point 3:\n";
            cin >> x >> y >> z;
            Point C(x, y, z);
            Triangle tria(A, B, C);
            cout << "Select an action:\n1 - Triangle type\n2 - Triangle area\n3 - Triangle perimeter\n4 - Triangle centroid\n";
            cout << "5 - Is a point part of the triangle plane inside it\n6 - Is a point part of the triagle plane outside it\n7 - Is a point lieing on one of the triangle's sides\n";
            cin >> n2;
            while (n2 < 1 || n2 > 7)
            {
                cout << "Please enter a valid option!\n";
                cin >> n2;
            }
            switch (n2)
            {
            case 1:
            {
                cout << "The type of the triangle is: " << tria.getType() << endl;
            }
            break;
            case 2:
            {
                cout << "The area of the triangle is: " << tria.calculateArea() << endl;
            }
            break;
            case 3:
            {
                cout << "The perimeter of the triangle is: " << tria.calculatePerimeter() << endl;
            }
            break;
            case 4:
            {
                cout << "The centroid of the triangle is: " << tria.getCentroid() << endl;
            }
            break;
            case 5:
            {
                cout << "Enter point coordinates:\n";
                cin >> x >> y >> z;
                Point point_tmp(x, y, z);
                cout << "The point is part of the triangle plane and is inside it: " << boolalpha << (tria < point_tmp) << endl;
            }
            break;
            case 6:
            {
                cout << "Enter point coordinates:\n";
                cin >> x >> y >> z;
                Point point_tmp(x, y, z);
                cout << "The point is part of the triangle plane and is outside it: " << boolalpha << (tria > point_tmp) << endl;
            }
            break;
            case 7:
            {
                cout << "Enter point coordinates:\n";
                cin >> x >> y >> z;
                Point point_tmp(x, y, z);
                cout << "The point is lying on one of the tirangle's sides: " << boolalpha << (tria == point_tmp) << endl;
            }
            break;
            }
            break;
        }
         case 6: {
                cout << "Please enter coordinates of 4 points to create a tetrahedron!\n";
                cout << "Point 1:\n";
                cin >> x >> y >> z;
                Point A(x, y, z);
                cout << "Point 2:\n";
                cin >> x >> y >> z;
                Point B(x, y, z);
                cout << "Point 3:\n";
                cin >> x >> y >> z;
                Point C(x, y, z);
                cout << "Point 4:\n";
                cin >> x >> y >> z;
                Point D(x, y, z);
                Tetrahedron tetra(A, B, C, D);
                cout
                        << "Select an action:\n1 - Check if tetrahedron is regular\n2 - Check if tetrahedron is orthogonal\n3 - Finding the surrounding surface\n4 - Tetrahedron volume\n";
                cout
                        << "5 - Is a point part of the tetrahedron plane inside it\n6 - Is a point part of the tetrahedron plane outside it\n7 - Is a point lying on one of the tetrahedron's sides\n";
                cin >> n2;
                while (n2 < 1 || n2 > 7) {
                    cout << "Please enter a valid option!\n";
                    cin >> n2;
                }
                switch (n2) {
                    case 1: {
                        cout << "Is the tetrahedron regular: " << boolalpha << tetra.isRegular() << endl;
                    }
                        break;
                    case 2: {
                        cout << "Is the tetrahedron orthogonal: " << boolalpha << tetra.isOrthogonal() << endl;
                    }
                        break;
                    case 3: {
                        cout << "The surrounding surface: " << boolalpha << tetra.calculateSurroundingArea() << endl;
                    }
                        break;
                    case 4: {
                        cout << "Tetrahedron volume: " << boolalpha << tetra.calculateVolume() << endl;
                    }
                        break;
                    case 5: {
                        cout << "Enter point coordinates:\n";
                        cin >> x >> y >> z;
                        Point point_tmp(x, y, z);
                        cout << "The point is part of the tetrahedron plane and is inside it: " << boolalpha
                             << (tetra < point_tmp) << endl;
                    }
                        break;
                    case 6: {
                        cout << "Enter point coordinates:\n";
                        cin >> x >> y >> z;
                        Point point_tmp(x, y, z);
                        cout << "The point is part of the tetrahedron plane and is outside it: " << boolalpha
                             << (tetra > point_tmp) << endl;
                    }
                        break;
                    case 7: {
                        cout << "Enter point coordinates:\n";
                        cin >> x >> y >> z;
                        Point point_tmp(x, y, z);
                        cout << "The point is lying on one of the tetrahedron's sides: " << boolalpha
                             << (tetra == point_tmp) << endl;
                    }
                        break;
                }
                break;

            }
        
        }

    }
    if (n == 2)
    {
        fstream fin("input.txt");
        if (!fin) {
            cerr << "Could not open file!" << endl;
            return 1;
        }
        int n;
        int n2;
        int x, y, z;

        do
        {
            cout << "Select geometric figure:\n1 - Point\n2 - Vector\n3 - Line\n4 - Segment\n5 - Triangle\n6 - Tetrahedron\n";
            fin >> n;
        } while (n < 1 || n > 6);
        switch (n)
        {
        case 1:
        {

            cout << "Please enter x, y, z:" << endl;
            fin >> x >> y >> z;
            Point point1(x, y, z);
            Point point_def();

            cout << "Do you want to enter another point?(y/n)" << endl;
            char ans;
            fin >> ans;
            while (ans != 'y' && ans != 'n')
            {
                cout << "Please enter y for yes or n for no!" << endl;
                fin >> ans;
            }
            if (ans == 'y')
            {
                cout << "Please enter coordinates for second point!" << endl;
                fin >> x >> y >> z;
            }
            Point point2(x, y, z);
            cout << point1.getX() << point1.getY() << point1.getZ() << endl;
            cout << point2.getX() << point2.getY() << point2.getZ() << endl;
            break;
        }
        case 2:
        {
            Vector vector1;
            cout << "Please choose how to identify a vector?\n1 - Via vector coordinates\n2 - Via two points" << endl;
            fin >> n2;
            while (n2 < 1 || n2 > 2)
            {
                cout << "Please enter a valid option!\n";
                fin >> n2;
            }
            if (n2 == 1)
            {
                cout << "Please enter vector coordinates!\n";
                fin >> x >> y >> z;
                Vector tmp(x, y, z);
                vector1 = tmp;
            }
            else
            {
                cout << "Please enter coordinates for two points!\nPoint1:\n";
                fin >> x >> y >> z;
                Point point_start(x, y, z);
                cout << "Point2:\n";
                fin >> x >> y >> z;
                Point point_end(x, y, z);
                Vector vec_temp(point_start, point_end);
                vector1 = vec_temp;
            }

            cout << "Select an action:\n1 - Vector length calculation\n2 - Vector direction calculation\n3 - Zero vector check\n4 - Checking for parallelism of two vectors\n5 - Perpendicularity check of two vectors\n6 - Collecting two vectors\n"
                << "7 - Subtraction of two vectors\n8 - Multiplication of a vector by a real number\n9 - Scalar product of two vectors\n10 - Vector product of two vectors\n11 - Mixed product of three vectors\n";
            fin >> n2;
            while (n2 < 1 || n2 > 11)
            {
                cout << "Please enter a valid option!\n";
                fin >> n2;
            }

            switch (n2)
            {
            case 1:
                cout << "The length of: " << vector1 << ", is: " << vector1.calculateLength() << endl;
                break;
            case 2:
                try
                {
                    cout << vector1.calculateDirection() << endl;
                }
                catch (VectorLengthException& vec_ex)
                {
                    cerr << "Vector length = 0 \n";
                }
                break;
            case 3:
                cout << boolalpha << vector1.isZeroVector() << endl;
                break;
            case 4:
            {
                cout << "Please, enter coordinates for a vector: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                try
                {
                    cout << boolalpha << vector1.isParallelWith(vector2) << endl;
                }
                catch (VectorLengthException& vec_ex)
                {
                    cerr << "Vector length = 0 \n";
                }
                break;
            }
            case 5:
            {
                cout << "Please, enter coordinates for a vector: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                try
                {
                    cout << boolalpha << vector1.isPerpendicularWith(vector2) << endl;
                }
                catch (VectorLengthException& vec_ex)
                {
                    cerr << "Zero vector\n";
                }
                break;
            }

            case 6:
            {
                cout << "Please enter vector coordinates: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << *(vector1 + vector2) << endl;
                break;
            }
            case 7:
            {
                cout << "Please enter vector coordinates: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << *(vector1 - vector2) << endl;
                break;
            }

            case 8:
            {
                cout << "Please enter a number: " << endl;
                fin >> x;
                cout << *(vector1 * x) << endl;
                break;
            }
            case 9:
            {
                cout << "Please enter vector coordinates: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << vector1 * vector2 << endl;
                break;
            }
            case 10:
            {
                cout << "Please enter vector coordinates: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << (vector1 ^ vector2) << endl;
                break;
            }
            case 11:
            {
                cout << "Please enter coordinates for two vectors: \nVector 1: " << endl;
                fin >> x >> y >> z;
                Vector vector2(x, y, z);
                cout << "Vector 2 :" << endl;
                fin >> x >> y >> z;
                Vector vector3(x, y, z);
                cout << vector1(vector2, vector3) << endl;
                break;
            }
            }
            break;
        }
        case 3:
        {
            cout << "Please choose how to identify a line?\n1 - Via vector and point\n2 - Via two points" << endl;
            fin >> n2;
            Line line_1;
            while (n2 < 1 || n2 > 2)
            {
                cout << "Please enter a valid option!\n";
                fin >> n2;
            }
            if (n2 == 1)
            {
                cout << "Please enter coordinates for point and vector!\nPoint:\n";
                fin >> x >> y >> z;
                Point point(x, y, z);
                cout << "Vector:\n";
                fin >> x >> y >> z;
                Vector vec_1(x, y, z);
                Line line_temp(point, vec_1);
                line_1 = line_temp;
            }
            else
            {
                cout << "Please enter coordinates for two points!\nPoint 1:\n";
                fin >> x >> y >> z;
                Point point_start(x, y, z);
                cout << "Point 2:\n";
                fin >> x >> y >> z;
                Point point_end(x, y, z);
                Line line_temp(point_start, point_end);
                line_1 = line_temp;
            }
            cout << "Select an action:\n1 - Lines direction\n2 - A normal vector\n3 - An angle between two lines\n4 - A point lies on another line\n5 - Parallel lines\n6 - Line matching\n7 - Intersects lines\n8 - Crossed lines\n9 - Perpendicular lines\n";
            fin >> n2;
            while (n2 < 1 || n2 > 9)
            {
                cout << "Please enter a valid option!\n";
                fin >> n2;
            }
            switch (n2)
            {
            case 1:
            {
                //Vector vec_tmp = line1.getDirection();
                cout << "The direction of the line "
                    << " is " << line_1.getDirection() << endl;
            }
            break;
            case 2:
            {
                cout << "The normal vector of the given line is: " << line_1.getNormal() << endl;
            }
            break;
            case 3:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The angle btween the two lines is: " << line_1.getAngle(line2) << " rad.\n";
            }
            break;
            case 4:
            {
                cout << "Eneter a point:\n";
                fin >> x >> y >> z;
                Point point_lies(x, y, z);
                cout << "The point lies on the line: " << boolalpha << line_1 + point_lies << endl;
            }
            break;
            case 5:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The lines are parallel: " << boolalpha << (line_1 || line2) << endl;
            }
            break;
            case 6:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The l;ines overlap: " << boolalpha << (line_1 == line2);
            }
            break;
            case 7:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The lines are intersecting: " << boolalpha << (line_1 && line2) << endl;
            }
            break;
            case 8:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "The lines are crossing: " << boolalpha << (line_1 != line2);
            }
            break;
            case 9:
            {
                Line line2 = second_line(x, y, z, n2);
                cout << "Line 1 is perpendicular to line 2: " << boolalpha << (line_1 | line2) << endl;
            }
            break;
            }
            break;
        }
        case 4:
        {
            cout << "Please enter point and vector coordinates!\nPoint:\n";
            fin >> x >> y >> z;
            Point point(x, y, z);
            cout << "Vector:\n";
            fin >> x >> y >> z;
            Vector vec_1(x, y, z);
            //Line line_temp(point, vec_1);
            cout << "Select an action:\n1 - Segment length\n2 - Segment middle point\n3 - Check if a point is a part of the given segment\n";
            fin >> n2;
            while (n2 < 1 || n2 > 3)
            {
                cout << "Please enter a valid option!\n";
                fin >> n2;
            }
            Segment seg1(point, vec_1);
            switch (n2)
            {
            case 1:
            {
                cout << "The length of the segment is: " << seg1.getLen() << endl;
            }
            break;
            case 2:
            {
                //Point mid = seg1.getMiddle();
                cout << "The middle point of the segment is: " << seg1.getMiddle();
            }
            break;
            case 3:
            {
                //Point partPoint;
                cout << "Enter point coordinates!\n";
                fin >> x >> y >> z;
                Point partPoint(x, y, z);
                cout << "The point " << partPoint << " is part of the segment " << seg1 << ": " << boolalpha << (seg1 == partPoint) << endl;
            }

            break;
            }

            break;
        }
        case 5:
        {
            cout << "Please eneter coordinates of 3 points to create a triangle!\n";
            cout << "Point 1:\n";
            fin >> x >> y >> z;
            Point A(x, y, z);
            cout << "Point 2:\n";
            fin >> x >> y >> z;
            Point B(x, y, z);
            cout << "Point 3:\n";
            fin >> x >> y >> z;
            Point C(x, y, z);
            Triangle tria(A, B, C);
            cout << "Select an action:\n1 - Triangle type\n2 - Triangle area\n3 - Triangle perimeter\n4 - Triangle centroid\n";
            cout << "5 - Is a point part of the triangle plane inside it\n6 - Is a point part of the triagle plane outside it\n7 - Is a point lieing on one of the triangle's sides\n";
            fin >> n2;
            while (n2 < 1 || n2 > 7)
            {
                cout << "Please enter a valid option!\n";
                fin >> n2;
            }
            switch (n2)
            {
            case 1:
            {
                cout << "The type of the triangle is: " << tria.getType() << endl;
            }
            break;
            case 2:
            {
                cout << "The area of the triangle is: " << tria.calculateArea() << endl;
            }
            break;
            case 3:
            {
                cout << "The perimeter of the triangle is: " << tria.calculatePerimeter() << endl;
            }
            break;
            case 4:
            {
                cout << "The centroid of the triangle is: " << tria.getCentroid() << endl;
            }
            break;
            case 5:
            {
                cout << "Enter point coordinates:\n";
                fin >> x >> y >> z;
                Point point_tmp(x, y, z);
                cout << "The point is part of the triangle plane and is inside it: " << boolalpha << (tria < point_tmp) << endl;
            }
            break;
            case 6:
            {
                cout << "Enter point coordinates:\n";
                fin >> x >> y >> z;
                Point point_tmp(x, y, z);
                cout << "The point is part of the triangle plane and is outside it: " << boolalpha << (tria > point_tmp) << endl;
            }
            break;
            case 7:
            {
                cout << "Enter point coordinates:\n";
                fin >> x >> y >> z;
                Point point_tmp(x, y, z);
                cout << "The point is lieing on one of the tirangle's sides: " << boolalpha << (tria == point_tmp) << endl;
            }
            break;
            }
            break;
        }
        }

    }


    return 0;

    // Point p1(1, 2, 3);
    // Point p2(p1);

    // cout << p.getX() << endl;
    // cout << p.getY() << endl;
    // cout << p.getZ() << endl;

    // cout << p1.getX() << endl;
    // cout << p1.getY() << endl;
    // cout << p1.getZ() << endl;

    // cout << p2.getX() << endl;
    // cout << p2.getY() << endl;
    // cout << p2.getZ() << endl;
    //
    //
    //    if(true){
    //        Vector vec(1, 2, 3);
    //        cout << "vec length = " << vec.getLength() << endl;
    //
    //        try{
    //            // prints the direction vector
    //            vector<double> * dir = vec.calculateDirection();
    //            cout << "direction vector = ( ";
    //            for(int i = 0; i < 3; i++){
    //                cout << dir->at(i) << (i == 2 ? " " : ", ");
    //            }
    //            cout << " )" << endl;
    //        }catch(const std::exception& e){
    //            std::cerr << e.what() << '\n';
    //        }
    //
    //        cout << (vec.isZeroVector() ? "is zero vector" : "isn't zero vector") << endl;
    //    }
    //
    //    // the inner objects are destroyed appropriately when exiting scope before printing end
    //    cout << "end" << endl;
    //
    //    Vector vec1(-3, -2, 5);
    //    Vector vec2(2, 4/3, -(10/3));  // perpandicular vector to vec1
    //    Vector vec3(3, 2, 2); // parallel vector to vec1
    //    Vector vec4(1, 2, 3);
    //
    //    cout << (vec1.isParallelWith(vec2) ? "the vectors are parallel" :
    //                                        "the vectors are not parallel") << endl;
    //
    //    cout << (vec1.isPerpendicularWith(vec2) ? "the vectors are perpendicular" :
    //                                        "the vectors are not perpendicular") << endl;
    //
    //    cout << (vec1 + vec2) << endl;
    //    cout << (vec1 - vec2) << endl;
    //    cout << (vec1*3) << endl;
    //    cout << (vec1*vec2) << endl;
    //    cout << (vec1^vec2) << endl;
    //    cout << vec1(vec2, vec4) << endl;
}
