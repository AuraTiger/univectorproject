#include"Point.h"

Point::Point() : x(0), y(0), z(0) {}

Point::Point(const Point &point) : x(point.x), y(point.y), z(point.z) {}

Point::Point(double x, double y, double z) : x(x), y(y), z(z) {}

Point::~Point(){
//    std::cout << "des" << std::endl;
}

double Point::getX() const {
    return x;
}

double Point::getY() const {
    return y;
}

double Point::getZ() const {
    return z;
}

bool Point::operator==(const Point &p1) const {
    return this->x == p1.x && this->y == p1.y && this->z == p1.z;
}

std::ostream &operator<<(std::ostream &out, const Point &pnt) {
    out << "Point{ x = " << pnt.getX() << ", y = " << pnt.getY() << ", z = " << pnt.getZ() << " }\n";
    return out;
}







