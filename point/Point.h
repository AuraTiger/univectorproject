#ifndef POINT
#define POINT

#include<iostream>

class Point{
    private:
        double x;
        double y;
        double z;
    public:
        Point();
        Point(const Point &);
        Point(double, double, double);
        virtual ~Point();
        double getX() const;
        double getY() const;
        double getZ() const;

        bool operator==(const Point&) const;

};

std::ostream &operator<<(std::ostream &, const Point &);

#endif // POINT
